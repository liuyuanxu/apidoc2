package com.apidoc.utis;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.apidoc.common.Const;

import java.io.*;

/**
 * json工具类
 */
public class JsonUtil {

    /**
     * 把对象转换成json字符串
     *
     * @param object 对象
     * @return String
     */
    public static String toJsonString(Object object) {
        return JSON.toJSONString(object);
    }

    /**
     * 把json文本保存到硬盘
     *
     * @param jsonStr  json文本
     * @param fileName 文件名
     * @return boolean
     */
    public synchronized static boolean writer(String fileName, String jsonStr) {
        //文件不存在时创建
        File newFile = createNewFile(Const.apidocPath + fileName);
        try (BufferedWriter bufferedWriter = new BufferedWriter(
                new OutputStreamWriter(new FileOutputStream(newFile), Const.charSet), Const.bufferSize)
        ) {
            bufferedWriter.write(jsonStr);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * 把object对象转成文本保存到硬盘
     *
     * @param object   对象
     * @param fileName 文件名
     * @return boolean
     */
    public synchronized static boolean writer(String fileName, Object object) {
        return writer(fileName, toJsonString(object));
    }

    /**
     * 创建新文件
     *
     * @param path 文件路径
     * @return File
     */
    public static File createNewFile(String path) {
        File file = new File(path);
        if (file.exists()) {
            return file;
        } else {
            //文件不存在时创建
            String parentPath = path.substring(0, path.lastIndexOf(File.separator));
//            System.out.println(parentPath);
            new File(parentPath).mkdirs();//创建父目录
            try {
                File f = new File(path);
                f.createNewFile();//创建文件
                return f;
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }
        }
    }

    /**
     * 读取json文件到内存，返回字符串
     *
     * @param fileName 文件名
     * @return String
     */
    public synchronized static String reader(String fileName) {
        File file = createNewFile(Const.apidocPath + fileName);
//        System.out.println(Const.apidocPath + fileName);
        try (BufferedReader bufferedReader = new BufferedReader(
                new InputStreamReader(new FileInputStream(file), Const.charSet), Const.bufferSize)
        ) {
            String line;
            StringBuilder sb = new StringBuilder();
            while ((line = bufferedReader.readLine()) != null) {
                sb.append(line);
            }
            return sb.toString();
        } catch (IOException e) {
            return null;
        }
    }

    /**
     * 读取文件数据并转换为json对象 JSONObject
     *
     * @param fileName 文件名
     * @return JSONObject json对象
     */
    public static JSONObject readerObject(String fileName) {
//        System.out.println(fileName);
        JSONObject object = JSON.parseObject(reader(fileName));
        return object == null ? new JSONObject() : object;
    }

}
