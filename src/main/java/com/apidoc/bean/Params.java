package com.apidoc.bean;

import java.util.List;

/**
 * 参数 请求参数和响应参数
 */
public class Params {
    private String type;//参数的请求或响应类型
    private String description;//描述
    private List<ParamItem> params; //参数集合
    private String methodUUID;//方法的唯一编号

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<ParamItem> getParams() {
        return params;
    }

    public void setParams(List<ParamItem> params) {
        this.params = params;
    }

    public String getMethodUUID() {
        return methodUUID;
    }

    public void setMethodUUID(String methodUUID) {
        this.methodUUID = methodUUID;
    }
}
