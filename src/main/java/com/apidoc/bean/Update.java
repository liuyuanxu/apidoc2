package com.apidoc.bean;

/**
 * 修改时用的 bean
 */
public class Update {
    private String name;//文件名
    private String content;//内容

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
