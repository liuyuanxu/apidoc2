package com.apidoc.bean;

/**
 * 文档的基本信息 bean
 */
public class Info {
    private String title = "这是标题"; //标题
    private String description = "  \n" +
            "这里填写您的项目描述（可为空）\n" +
            "    第一条：项目需求；\n" +
            "    第二条：可以换行；\n" +
            "    第二条：等等等等；"; //描述
    private String version = "1.0.0"; //版本

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }


}
