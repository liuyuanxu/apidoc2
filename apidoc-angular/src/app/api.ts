//api接口
export const apis={
  info:"/apidoc/info",
  modules:"/apidoc/modules",
  actions:"/apidoc/actions",
  detail:"/apidoc/detail",
  updateInfo: "/apidoc/updateInfo",
  updateAction: "/apidoc/updateAction",
  updateActionDescription: "/apidoc/updateActionDescription",
  updateDetail: "/apidoc/updateDetail",
  updateModulesSort: "/apidoc/updateModulesSort",
  updateActionsSort: "/apidoc/updateActionsSort",
};
